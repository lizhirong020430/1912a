### 怎么官埋又件

1.  -版本管理工具 -命令行工具

### 安装

1. windows 需要单独安装
2. mac 电脑自带的 ###检测安装成功
   shell
   git --version

# git version 2.32.0 (Apple Git-132)

### 怎么管理文件

1.git init
在当前目录产生 文件夹 不允许子目录中有 GIT 会嵌套管理

### 怎么提交

——本地工作区(本地文件夹)
——暂存区
——历史版本区(有版本号)
以上本地管理

——远程仓库 (第三方服务)

本地工作区-》(git add 路径/-A)暂存区-》(git commit -m "message 提交信息")历史版本区
/A 提交全部 . 当前目录

### 查看历史提交记录

git log

### 可视化工具

sourcetree

### 回滚

1.git reset --hard HEAD /commit id 直接回到回滚的 commitid 之后不在显示
2.git revert --no-commit HEAD /commit id 找到之前的文件状态 拉去之前文件状态 手动修改文件 得到你想要的文件状态 再次提交

### 查看工作区状态

git status



### 多人协作开发

-解决冲突  本地 选择保留那个  重新提交

手动解决 再次提交
git pull



-使用分支进行开发
创建开发分支，多人开发，创建自身开发分支也development
测试分支 test
预发分支 release
合到主分支   master


### 创建本地分支
shell 
git branch xxx

### 查看本地分支
git branch

### 查看所有分支
git branch --all

### 查看远程分支
git branch -r


### 切换分支
git checkout  day_lzr_8.13


### 切换查看分支
git checkout -b  day_lzr_8.13

### 合并分支
git merge 要合并的分支代码








