### git
- 版本管理工具
-命令行工具

### 安装
1. windows 需要单独安装
2. mac 电脑自带

### 检测安装成功
··· shell
git --version 


### 怎么管理文件

1. git init 
在当前目录下会产生.get文件夹，不允许子目录中有.git文件夹，会嵌套管理，管理所有目录

### 怎么提交管理记录

- 本地工作区（本地文件夹）
- 暂存区
- 历史版本区（产生文件提交的版本号）
 
以上是本地管理

- 远程仓库（第三方服务）


本地工作区 ->（git add 路径/-A） 暂存区 -> （git commit -m 'message提交信息' -> 历史版本区）

### 查看历史提交记录

git log

### 回滚文件状态

1. git reset --hard HEAD~6/commitId 直接回到回滚的commitId，之后多有记录不显示
2. git revert --no-commit HEAD~6/commitId 找到之前的文件状态，拉取之前的文件状态，手动修改文件，得到你想要的文件状态，再次提交
3. git status 查看工作区状态

### 可视化工具

sourcetree


###  写入什么什么

####  修改什么什么东西
