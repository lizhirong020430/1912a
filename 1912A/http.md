### http

超文本传输协议

针对浏览器和服务器进行通信 (短时)

1. 浏览器主动向服务器发请求
2. 服务器接受请求，作出响应
3. 作出响应自动断开协议

### 那些情况会发起请求

1.  浏览器 url 地址按回车，一般返回 document
2.  js <script src=""></script>
3.  css <link src=""></link>
4.  img <img src="" /> 图片 的 src
5.  媒体 <media src=""></media> <audio src=""></audio>
6.  font css font-family:''
    以上是静态资源 文件 同源策略限制 允许跨域
7.  fetch 和 XHR () ajax
    受同源策略限制，同协议+同域名同端口 受同源策略限制，同协议+同域名同端口

### 为什么会有跨域问题？

出于浏览器的同源策略限制。同源策略是一种约定，它是浏览器最核心也最基本的安全功能，如果缺少了同源策略，浏览器很容易受到 XSS、CSRF 等攻击。即便两个不同的域名指向同一个 ip 地址，也非同源。同源策略会阻止一个域的 javascript 脚本和另外一个域的内容进行交互。所谓同源（即指在同一个域）就是两个页面具有相同的协议（protocol），主机（host）和端口（port）。

### 跨域解决方案 https://blog.csdn.net/qq_42880714/article/details/122955087

1.  jsonp <script src=""></script> src 属性 只支持 GET 方法
2.  反向代理
    - 开发 webpack DevServer proxy
    - 生产 Nginx 的 post_pass
3.  cors 跨域资源分享
    - 服务器配置 headers 头 实现资源响应
    - 简单请求/非简单请求(体检发起预检请求 请求方式 option)
4.  postMessage
