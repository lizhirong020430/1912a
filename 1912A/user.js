[
  {
    "role": "admin",//管理员
    "viewList": [
      {//访问的页面
        "path": '/view',
        "title": '访问统计'
      },
      {
        "path": '/user',
        "title": '用户管理'
      },
      {//分类管理页面
        "path": '/article/category',
        "title": '分类管理'
      },
      {//标签页面
        "path": '/article/category',
        "title": '标签'
      }
    ],
    "apiList": [//页面 路由 方法
      {
        'path': '/view',
        'url': '/api/view/:id',
        'method': 'DELETE'
      },
      {//分类管理
        'path': '/article/category',
        'url': '/api/category',
        'method': 'POST'
      }
    ]
  },
  {
    "role": "visitor",//访客
    "viewList": [
      {//访问的页面
        "path": '/view',
        "title": '访问统计'
      },{//分类管理页面
        "path": '/article/category',
        "title": '分类管理'
      }
    ],
    "apiList": [
      
    ]
  },
]