console.log(1);
setTimeout(() => {
    console.log(4);
    Promise.resolve(8).then(res=>{
        console.log(res);
        setTimeout(() => {
            console.log(10);
        }, 0);
    })
    
}, 0);
new Promise((resolve, reject) => {
    console.log(2);
    setTimeout(() => {
        resolve(3);
    }, 10);
    reject(11)
    // resolve(3);
}).then(res => {
    console.log(res);
}).catch(error=>{
    console.log(error);
})

console.log(6);

// 1 2 6 3 4 
//promise 里面是同步  .then .cat 是异步
// setTimeout 宏任务


//先微 后宏
// 1 2 6 4 8 10 3 
//1 2 6 11 4 8 10

//eventloop 的题